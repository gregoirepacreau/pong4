package pong;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.scene.shape.Rectangle;

public class Main extends Application implements BallCallBack
{
    private Racket leftRacket;
    private Racket rightRacket;

    private int width = 800;
    private int height = 600;

    private int leftScore = 0;
    private int rightScore = 0;

    private Group root;

    @Override
    public void start(Stage stage) throws Exception
    {
        Ball ball = new Ball(width, height, 200, 200, 0.25, 0.25);
        ball.setBallCallBack(this);

        //Creating a Group object
        Group root = new Group(ball);

        //Creating a scene object
        Scene scene = new Scene(root, width, height, Color.BLACK);

        //Creating the score text object
        Text score = new Text(width/2-61, height-10, "0 - 0");
        score.setFill(Color.WHITE);
        score.setFont(Font.font("Verdana", 50));

        //adding a line in the middle
        Rectangle middleLine = new Rectangle(width/2-2, 0,4, height-50);
        middleLine.setFill(Color.GREY);


        leftRacket = new AIRacket(width, true, ball);
        rightRacket = new HumanRacket(width, false, scene);

        root.getChildren().add(leftRacket);
        root.getChildren().add(rightRacket);
        root.getChildren().add(middleLine);
        root.getChildren().add(score);

        //Setting title to the Stage
        stage.setTitle("Pong");

        //Adding scene to the stage
        stage.setScene(scene);

        //Displaying the contents of the stage
        stage.show();

        ball.play();
    }


    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void ballHit(boolean left, double height)
    {
        boolean caught;
        if (left)
        {
            caught = leftRacket.isInside(height);
        }
        else
        {
            caught = rightRacket.isInside(height);
        }
        if (!caught)
        {
            System.out.print("\n");
            if (left)
            {
                leftScore = leftScore + 1;
            }
            else
            {
                rightScore = rightScore + 1;
            }

            System.out.print(leftScore);
            System.out.print(" - ");
            System.out.print(rightScore);
        }
    }
}
