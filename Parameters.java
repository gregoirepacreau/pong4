package pong;

public class Parameters
{
    public static int racket_width = 10;
    public static int racket_height = 100;
    public static double inputDelta = 50;
    public static double aiTickRate = 0.1;
    public static double ballRadius = 10;
}